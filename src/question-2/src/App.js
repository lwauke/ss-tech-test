import React, { useEffect, useRef, useState } from 'react';
import style from './App.module.scss';
import Card from './components/Card';

function App() {
  const [facts, setFacts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [requestError, setRequestError] = useState(false);
  const mounted = useRef(false);

  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true; // avoid calling the request twice
      fetch('https://catfact.ninja/facts?limit=9')
        .then(res => res.json())
        .then(factsResponse => setFacts(factsResponse?.data))
        .finally(() => setLoading(false))
        .catch(() => setRequestError(true));
    }
  }, []);

  return (
    <>
      <h2 className={style.app__title}>List of facts</h2>
      <main className={style.app__main}>
        { loading && 'loading'}
        { requestError && 'Error while trying to get facts'}
        {facts.map(({ fact, length }) => (
          <Card
            text={fact}
            key={`${fact.slice(0, 10)}-${length}`}
          />
        ))}
      </main>
    </>
  );
}

export default App;

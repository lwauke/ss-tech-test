import React from 'react';
import { render, screen } from '@testing-library/react';
import Card from './index';

describe('Card', () => {
  it('should render text', () => {
    render(<Card text='lorem ipsum' />);
    const card = screen.getByText(/lorem ipsum/i);

    expect(card).not.toBeNull();
  });
  it('should not render component when has no text', () => {
    const { container } = render(<Card text='' />);

    expect(container.firstChild).toBeNull();
  });
});
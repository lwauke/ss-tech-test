import React from 'react';
import PropTypes from 'prop-types';
import style from './index.module.scss';

function Card ({ text  }) {
  if (!text) {
    return null;
  }

  return <div className={style.card}>
    <h3 className={style.card__subtitle}>Fact:</h3>
    <p className={style.card__text}>{ text }</p>
  </div>;
}

Card.propTypes = {
  text: PropTypes.string
};

export default Card;
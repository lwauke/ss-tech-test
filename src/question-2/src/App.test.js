import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import App from './App';

describe('app', () => {
  it('should render fact api response', async () => {
    const response = {
      data: [
        {
          fact: 'some random fact',
          length: 123
        }
      ]
    };
    global.fetch = jest
      .fn()
      .mockReturnValueOnce(
        Promise.resolve({
          json: () => Promise.resolve(response)
        })
      );

    render(<App />);
    await waitFor(() => {
      const card = screen.getByText(/some random fact/i);
      expect(card).toBeInTheDocument();
    });
  });
  it('should render error message when request fails', async () => {
    global.fetch = jest
      .fn()
      .mockReturnValueOnce(Promise.reject());

    render(<App />);
    await waitFor(() => {
      const errMessage = screen.getByText(/error while trying to get facts/i);
      expect(errMessage).toBeInTheDocument();
    });
  });
});

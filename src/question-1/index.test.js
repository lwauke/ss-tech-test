const revert = require('./index.js');

describe('Revert function', () => {
  it('Should reverts array of any values', () => {
    const arr = [1, 'random value', 0, {}];
    const result = revert(arr);

    expect(arr.reverse()).toEqual(result);
  });

  it('Should handle null', () => {
    const result = revert(null);

    expect(result).toEqual([]);
  });

  it('Should handle undefined', () => {
    const result = revert(undefined);

    expect(result).toEqual([]);
  });
})
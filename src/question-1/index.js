const revert = arr => Array.prototype.reduceRight.call(
  arr || [],
  (acc, act) => [...acc, act],
  []
);

module.exports = revert;